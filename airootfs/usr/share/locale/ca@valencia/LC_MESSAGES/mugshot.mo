��            )   �      �     �     �  
   �     �     �     �     �               %  	   .     8     J     a  0   h     �     �  	   �     �     �     �     �  1   �  T         j     �  5   �  ?   �  �       �     �  
   �  
   �     �               !     >  
   W  
   b     m     �  	   �  6   �     �     �     �  	   	  
             8  >   V  j   �  .    	  )   /	  9   Y	  >   �	                                                     	                                              
                                       <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me Browse… Capture - Mugshot Capture from camera… Center Configure your profile image and contact details Left Mugshot Password: Retry Right Select a photo… Select from stock… Show debug messages (-vv debugs mugshot_lib also) This is a security measure to prevent unwanted updates
to your personal information. Update LibreOffice user details? Update Pidgin buddy icon? Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-07-22 19:12+0000
Last-Translator: Sergio Baldoví <serbalgi@gmail.com>
Language-Team: Catalan (Valencian) <ca@valencia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>Retalla</b> <b>Correu electrònic</b> <b>Fax</b> <b>Nom</b> <b>Telèfon de casa</b> <b>Inicials</b> <b>Cognom</b> <b>Telèfon de l'oficina</b> <b>Previsualització</b> Quant a mi Explora... Captura d'imatges - Mugshot Captura des d'una càmera... Al centre Configureu l'imatge del perfil i les dades de contacte A l'esquerra Mugshot Contrasenya: Reintenta A la dreta Seleccioneu una imatge... Selecciona des de mostrari... Mostra missatges de depuració (-vv també depura mugshot_lib) Esta és una mesura de seguretat per a previndre actualitzacions
no desitjades de la informació personal. Actualitzar les dades d'usuari en LibreOffice? Actualitzar la icona de l'amic en Pidgin? Desitgeu també actualitzar la icona de l'amic en Pidgin? Desitgeu també actualitzar les dades d'usuari en LibreOffice? 