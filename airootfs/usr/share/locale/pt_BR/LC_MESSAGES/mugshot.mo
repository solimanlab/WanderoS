��    )      d  ;   �      �     �     �  
   �     �     �     �     �     
          -  &   6     ]     w  	   �     �     �     �     �  0   �  4      +   5      a     �     �     �     �  	   �     �     �     �     �  1   �  D     T   X  �   �      S     t     �  5   �  ?   �  ~  #     �	     �	  
   �	     �	     �	     �	      
     
     +
  	   =
  *   G
     r
     �
     �
     �
     �
  
   �
     �
  /   �
  7   !  ;   Y  #   �     �     �     �     �     �     �  
   �     �       >   "  L   a  d   �  �     +   �     �  1     3   L  D   �                   &   !      	       $   
                                   "   (   '             )                                  #                %                                                         <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me An error occurred when saving changes. Authentication cancelled. Authentication failed. Browse… Cancel Capture - Mugshot Capture from camera… Center Configure your profile image and contact details Enter your password to
perform administrative tasks. Enter your password to change user details. Incorrect password... try again. Left Mugshot OK Password Required Password: Retry Right Select a photo… Select from stock… Show debug messages (-vv debugs mugshot_lib also) The application '%s' lets you
modify essential parts of your system. This is a security measure to prevent unwanted updates
to your personal information. This stable release improves Mugshot functionality for LDAP users, and includes the latest SudoDialog, improving the appearance and usability of the password dialog. Update LibreOffice user details? Update Pidgin buddy icon? User details were not updated. Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-18 22:01+0000
Last-Translator: Adriano Ramos <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>Cortar</b> <b>Email</b> <b>Fax</b> <b>Nome</b> <b>Telefone Residencial</b> <b>Iniciais</b> <b>Sobrenome</b> <b>Telefone Comercial</b> <b>Visualizar</b> Sobre Mim Ocorreu um erro ao salvar as alterações. Autenticação cancelada. Falha na autenticação. Procurar nos arquivos... Cancelar Captura - Mugshot Câmera... Centralizado Configure sua imagem de perfil e dados da conta Insira sua senha para
realizar tarefas administrativas. Insira sua senha para alterar as informações de usuário. Senha incorreta... tente novamente. À Esquerda Mugshot OK Senha Requerida Senha: Repetir À Direita Escolha uma foto... Escolha uma foto... Mostrar mensagens de depuração (-vv debugs mugshot_lib also) A aplicação '%s' permite que você
modifique partes essenciais do sistema. Essa é uma medida de segurança para prevenir mudanças indesejadas
em suas informações pessoais. Esta versão estável melhora a funcionalidade Mugshot para usuários LDAP e inclui a mais recente versão do SudoDialog, melhorando a aparência e usabilidade da caixa de diálogo de senha. Atualizar dados de usuário do LibreOffice? Atualizar imagem do Pidgin? Informações do usuário não foram atualizadas. Gostaria também de atualizar sua imagem do Pidgin? Gostaria também de atualizar seus dados de usuário no LibreOffice? 