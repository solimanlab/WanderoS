��    #      4  /   L           	       
   *     5     G     Y     i     z     �     �     �     �  	   �     �     �     �  0     4   7  +   l      �     �     �  	   �     �     �     �  1   �  D     T   `      �     �     �  5     ?   E  �  �       !   $     F     V      d      �     �  %   �     �     �  8   	  8   =	     v	     �	      �	     �	  H   �	  k   
  i   ~
  F   �
     /     8     D     T  
   p      {  y   �  c     �   z  X     I   ^  <   �  f   �  z   L                             
                 !   	                           "                                   #                                              <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me Authentication cancelled. Authentication failed. Browse… Cancel Capture from camera… Center Configure your profile image and contact details Enter your password to
perform administrative tasks. Enter your password to change user details. Incorrect password... try again. Left OK Password: Retry Right Select a photo… Show debug messages (-vv debugs mugshot_lib also) The application '%s' lets you
modify essential parts of your system. This is a security measure to prevent unwanted updates
to your personal information. Update LibreOffice user details? Update Pidgin buddy icon? User details were not updated. Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-02-28 20:54+0000
Last-Translator: Саша Петровић <salepetronije@gmail.com>
Language-Team: Serbian <sr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>Опсеци</b> <b>Адреса е-поште</b> <b>Факс</b> <b>Име</b> <b>Кућни телефон</b> <b>почетна слова</b> <b>Презиме</b> <b>Телефон на послу</b> <b>Преглед</b> О мени Потврда овлашћења је отказана. Потврда овлашћења није успела. Разгледај… Откажи Сними камерицом... Средина Подесите своју слику и податке о налогу Унесите своју лозинку ради
обављања управљачких задатака. Унесите своју лозинку ради промене корисничких података. Лозинка је нетачна... Покушајте поново. Лево У реду Лозинка: Покушај поново Десно Изаберите слику... Прикажи поруке о грешкама (-vv приказује и грешке библиотеке mugshot_lib) Програм „%s“ омогућава
измену кључних делова система. Ово је безбедносна мера ради спречавања нежељених промена
личних података. Да ли да освежим корисничке податке Либре Офиса? Да ли да освежим иконицу другара Пиџина? Кориснички подаци нису освежени. Да ли желите да освежите своју иконицу другара у Пиџину? Да ли би желели да освежите своје корисничке податке у Либре Офису? 