��          �      l      �     �  
   �               %     5     F  &   Z     �  0   �  +   �     �            T   1      �     �     �  5   �  ?     p  V     �  
   �  
   �     �               '  2   >     q  3   �  :   �     �            \   1  +   �     �  &   �  8      I   9                                      	                                                        
    <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> An error occurred when saving changes. Authentication cancelled. Configure your profile image and contact details Enter your password to change user details. Lightweight user configuration Mugshot Select a photo… This is a security measure to prevent unwanted updates
to your personal information. Update LibreOffice user details? Update Pidgin buddy icon? User details were not updated. Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-03-03 16:44+0000
Last-Translator: zvacet <ikoli@yahoo.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>Email adresa</b> <b>Fax</b> <b>Ime</b> <b>Kućni telefon</b> <b>Inicijali</b> <b>Prezime</b> <b>Uredski telefon</b> Došlo je do pogreške prilikom spremanja izmjena. Ovjera otkazana. Konfigurirajte sliku profila i pojedinosti kontakta Unesite vašu lozinku za izmjenu korisničkih pojedinosti. Laka korisnička konfiguracija Mugshot Odaberite fotografiju... Ovo je sigurnosna mjera za sprječavanje neželjenih ažuriranja
vaših osobnih informacija. Ažurirati LibreOffice korisničke detalje? Ažurirati Pidgin buddy ikonu? Pojedinosti korisnika nisu ažurirane. Želite li također ažurirati svoju Pidgin buddy ikonu? Želite li također ažurirati vaše korisničke detalje u LibreOffice-u? 