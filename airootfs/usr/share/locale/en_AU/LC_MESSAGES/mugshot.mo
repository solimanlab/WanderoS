��            )   �      �     �     �  
   �     �     �     �     �               %  	   .     8     J     a  0   h     �     �  	   �     �     �     �     �  1   �  T         j     �  5   �  ?   �  �       �     �  
   �     �     �     �               (     7  	   @     J     \     s  0   z     �     �  	   �     �     �     �     �  1   �  T   '      |     �  5   �  ?   �                                                     	                                              
                                       <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me Browse… Capture - Mugshot Capture from camera… Center Configure your profile image and contact details Left Mugshot Password: Retry Right Select a photo… Select from stock… Show debug messages (-vv debugs mugshot_lib also) This is a security measure to prevent unwanted updates
to your personal information. Update LibreOffice user details? Update Pidgin buddy icon? Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-07-19 10:01+0000
Last-Translator: Jackson Doak <noskcaj@ubuntu.com>
Language-Team: English (Australia) <en_AU@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me Browse… Capture - Mugshot Capture from camera… Centre Configure your profile image and contact details Left Mugshot Password: Retry Right Select a photo… Select from stock… Show debug messages (-vv debugs mugshot_lib also) This is a security measure to prevent unwanted updates
to your personal information. Update LibreOffice user details? Update Pidgin buddy icon? Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? 