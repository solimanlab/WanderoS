��    '      T  5   �      `     a     m  
   �     �     �     �     �     �     �     �  &   �     %     ?  	   V     `     g     y     �  0   �  4   �  +   �      )     J     O     W     Z  	   l     v     |     �  1   �  D   �  T         `     �     �  5   �  ?   �  t  0     �     �     �  
   �     �     �     
	     	     2	     C	  .   J	     y	     �	     �	  	   �	     �	     �	  	   �	  2   �	  6   
  <   T
  #   �
     �
     �
     �
     �
     �
     �
     �
     �
  F   
  E   Q  V   �  /   �  "     .   A  :   p  A   �                                            
   '            #                            %         "                  !                                     	   &         $              <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me An error occurred when saving changes. Authentication cancelled. Authentication failed. Browse… Cancel Capture - Mugshot Capture from camera… Center Configure your profile image and contact details Enter your password to
perform administrative tasks. Enter your password to change user details. Incorrect password... try again. Left Mugshot OK Password Required Password: Retry Right Select a photo… Show debug messages (-vv debugs mugshot_lib also) The application '%s' lets you
modify essential parts of your system. This is a security measure to prevent unwanted updates
to your personal information. Update LibreOffice user details? Update Pidgin buddy icon? User details were not updated. Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-08-19 14:47+0000
Last-Translator: Dražen Matešić <Unknown>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>Obrez</b> <b>E-poštni naslov</b> <b>Faks</b> <b>Ime</b> <b>Domači telefon</b> <b>Začetnice</b> <b>Priimek</b> <b>Telefon v pisarni</b> <b>Predogled</b> O meni Napaka se je zgodila ob shranjevanju sprememb. Overitev preklicana. Overitev  spodletela. Prebrskaj ... Prekliči Zajem - Mugshot Zajem iz kamere ... Sredinsko Nastavite svojo sliko profila in podrobnosti stika Vpišite svoje geslo
za opravljanje skrbniških nalog. Vpišite svoje geslo za spreminjanje podrobnosti uporabnika. Napačno geslo ... Poskusite znova. Levo Mugshot V redu Zahtevano je geslo Geslo: Poskusi znova Desno Izberite sliko ... Pokaži sporočila razhroščevanja (-vv razhrošča tudi mugshot_lib) Program '%s' vam dovoli
spreminjanje ključnih delov vašega sistema. To je varnosti ukrep, ki prepreči nezaželene posodobitve
vaših osebnih podrobnosti. Posodobim LibreOffice podrobnosti o uporabniku? Posodobim ikono prijatelja Pidgin? Podrobnosti uporabnika niso bili posodobljeni. Ali želite posodobiti tudi svojo ikono prijatelja Pidgin? Ali želite posodobiti tudi podrobnosti uporabnika v LibreOffice? 