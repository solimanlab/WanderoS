��    ,      |  ;   �      �     �     �  
   �     �               )     :     N     ]  &   f     �     �  	   �     �     �     �     �  0   �  4   0  +   e      �     �     �     �  e   �     D     G  	   Y     c     i     o     �  �   �  k   '  1   �  D   �  T   
      _     �     �  5   �  ?   �    /	     �
     �
     �
     �
     �
               +     >     P  )   \     �     �     �     �     �     �  
   �  (     9   ,  5   f  &   �     �  "   �     �  �   �     w     z  	   �     �     �     �     �  �   �  �   �  M     C   d  j   �  )     !   =  "   _  6   �  <   �        "          )   %      	       *   
                                     +                  ,   $                              '         &      (         #                       !                        <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me An error occurred when saving changes. Authentication cancelled. Authentication failed. Browse… Cancel Capture - Mugshot Capture from camera… Center Configure your profile image and contact details Enter your password to
perform administrative tasks. Enter your password to change user details. Incorrect password... try again. Left Lightweight user configuration Mugshot Mugshot enables users to easily update personal contact information. With Mugshot, users are able to: OK Password Required Password: Retry Right Select a photo… Select from stock… Set account details stored in /etc/passwd (usable with the finger command) and optionally synchronize with their LibreOffice contact information Set the account photo displayed at login and optionally synchronize this photo with their Pidgin buddy icon Show debug messages (-vv debugs mugshot_lib also) The application '%s' lets you
modify essential parts of your system. This is a security measure to prevent unwanted updates
to your personal information. Update LibreOffice user details? Update Pidgin buddy icon? User details were not updated. Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-09-05 08:16+0000
Last-Translator: Pasi Lallinaho <pasi@shimmerproject.org>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>Rajaus</b> <b>Sähköpostiosoite</b> <b>Faksi</b> <b>Etunimi</b> <b>Kotipuhelin</b> <b>Nimikirjaimet</b> <b>Sukunimi</b> <b>Työpuhelin</b> <b>Esikatselu</b> Omat tiedot Muutoksia tallennettaessa tapahtui virhe. Todennus keskeytetty. Tunnistus epäonnistui. Selaa… Peruuta Kuvankaappaus - Mugshot Kuvankaappaus kamerasta... Keskitetty Muuta profiilikuvaasi ja yhteystietojasi Syötä salasanasi
suorittaaksesi ylläpidollisia toimia. Syötä salasanasi muuttaaksesi käyttäjätietojasi. Väärä salasana... yritä uudelleen. Vasen Kevyt käyttäjätietojen muokkain Valokuva Mugshotin avulla käyttäjät voivat päivittää henkilökohtaisia yhteystietoja helposti. Mugshotin avulla käyttäjät voivat: OK Salasana vaaditaan Salasana: Yritä uudelleen Oikea Valitse kuva... Valitse oletuskuvista... Asettaa tilin ominaisuuksia, joita säilytetään tiedostossa /etc/passwd (käytettävissä finger-komennolla) ja valinnaisesti synkronoida nämä tiedot LibreOfficen tietojen kanssa Asettaa tilin valokuvan, joka näytetään kirjautumisikkunassa ja valinnaisesti synkronoida tämän kuvan Pidginin kaverikuvakkeekseen Näytä virheilmoituset (-vv näyttää myös mugshot_libin virheilmoitukset) Sovellus '%s' sallii sinun
muokata järjestelmäsi olennaisia osia. Tämä on turvallisuustoimenpide, jolla estetään ei-toivotut päivitykset
henkilökohtaisiin tietoihisi. Päivitä LibreOfficen käyttäjätiedot? Päivitä Pidginin tuttavakuvake? Käyttäjätietoja ei päivitetty. Haluatko päivittää myös Pidginin tuttavakuvakkeen? Haluatko päivittää myös LibreOfficen käyttäjätietosi? 