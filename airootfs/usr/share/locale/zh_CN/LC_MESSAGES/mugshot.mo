��    2      �  C   <      H     I     U  
   j     u     �     �     �     �     �     �  &   �          '  	   >     H     O     a     x  0     4   �  +   �           2     7     V  e   ^     �     �  	   �     �     �     �       �     k   �  1     D   E  �   �  a   *	  u   �	  T   
  �   W
  O   �
  �   C      �     
     $  5   C  ?   y  �  �     =     K     e  
   s     ~     �  
   �     �     �  	   �     �     �       	              '     7     J  '   Q  ,   y  0   �  "   �     �            Y        y     �  	   �     �     �     �     �  �   �  _   c  0   �  7   �  s   ,  H   �  b   �  T   L  �   �  I   (  �   r  )        ,     H  -   g  :   �                   $   
   '           )       *         !          &         1   .      #                 0                              /       -       ,   2      +                       %          (                  "          	           <b>Crop</b> <b>Email Address</b> <b>Fax</b> <b>First Name</b> <b>Home Phone</b> <b>Initials</b> <b>Last Name</b> <b>Office Phone</b> <b>Preview</b> About Me An error occurred when saving changes. Authentication cancelled. Authentication failed. Browse… Cancel Capture - Mugshot Capture from camera… Center Configure your profile image and contact details Enter your password to
perform administrative tasks. Enter your password to change user details. Incorrect password... try again. Left Lightweight user configuration Mugshot Mugshot enables users to easily update personal contact information. With Mugshot, users are able to: OK Password Required Password: Retry Right Select a photo… Select from stock… Set account details stored in /etc/passwd (usable with the finger command) and optionally synchronize with their LibreOffice contact information Set the account photo displayed at login and optionally synchronize this photo with their Pidgin buddy icon Show debug messages (-vv debugs mugshot_lib also) The application '%s' lets you
modify essential parts of your system. This development release fixes a large number of bugs from previous releases. User properties that cannot be edited are now restricted to administrative users. This development release restores camera dialog functionality that with recent software versions. This development release upgrades the camera dialog to use Cheese and Clutter to display and capture the camera feed. This is a security measure to prevent unwanted updates
to your personal information. This release includes a number of code quality improvements, bug fixes, and translations. Mugshot can now be built and run in a minimal chroot environment. This stable release adds support for the latest GTK and GStreamer technologies. This stable release improves Mugshot functionality for LDAP users, and includes the latest SudoDialog, improving the appearance and usability of the password dialog. Update LibreOffice user details? Update Pidgin buddy icon? User details were not updated. Would you also like to update your Pidgin buddy icon? Would you also like to update your user details in LibreOffice? Project-Id-Version: mugshot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-09-10 01:38+0000
Last-Translator: 玉堂白鹤 <yjwork@qq.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-05-27 11:00+0000
X-Generator: Launchpad (build 18968)
 <b>裁剪</b> <b>电子邮件地址</b> <b>传真</b> <b>名</b> <b>家庭电话</b> <b>缩写</b> <b>姓</b> <b>办公电话</b> <b>预览</b> 关于我 保存更改时发生错误。 认证取消。 认证失败。 浏览... 取消 捕获- Mugshot 从相机捕捉... 中心 设置您的头像和详细联系信息 请输入密码以便 
执行管理任务。 输入您的密码以更改用户详细信息。 密码不正确... 再试一次。 左侧 轻量级用户配置 Mugshot Mugshot 使用户可以轻松更新个人联系信息。 通过 Mugshot，用户可以： 确定 需要密码 密码： 重试 右侧 选择一张照片... 从库存中选择... 设置存储在 / etc / passwd 中的帐户详细信息（可与 finger 命令一起使用），并可选择与其 LibreOffice 联系信息同步 设置登录时显示的帐户照片，并可选择将此照片与其 Pidgin 好友图标同步 显示调试消息 (-vv debugs mugshot_lib also) 应用程序 '%s' 想要修改
系统的关键部分。 该开发版本修复了以前版本中的大量错误。 无法编辑的用户属性现在仅限于管理用户。 这个开发版本恢复了最近软件版本的相机对话框功能。 该开发版本升级相机对话框以使用 Cheese 和 Clutter 来显示和捕捉相机输入。 这是一项安全措施，可防止对您的个人信息进行不必要的更新。 此版本包括许多代码质量改进，错误修复和翻译。 Mugshot 现在可以在最小的 chroot 环境中构建和运行。 这个稳定版本增加了对最新 GTK 和 GStreamer 技术的支持。 这个稳定的版本改进了 LDAP 用户的 Mugshot 功能，并包含最新的 SudoDialog，改进了密码对话框的外观和可用性。 更新 LibreOffice 用户的详细信息? 更新Pidgin好友图标？ 用户详细信息未更新。 您还想更新你的Pidgin好友图标吗？ 您还想更新 LibreOffice 中的用户详细信息吗？ 